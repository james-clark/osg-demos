# LSC/Virgo Examples For The Open Science Grid

Some documentation and HTCondor workflows to demonstrate OSG usage, as well as 
general use of CVMFS-hosted software and data

## Building And Deploying Containers
**Temporary** (to be moved to wiki)
```
➜  lalapps_frview git:(master) ✗ docker build -t containers.ligo.org/james-clark/osg-demos/lalsuite-v6.53:latest . 
Sending build context to Docker daemon  65.02kB
Step 1/5 : FROM containers.ligo.org/lscsoft/lalsuite/lalsuite-v6.53:stretch
 ---> f44941b49bbd
Step 2/5 : ARG version
 ---> Using cache
 ---> 2235febcff6e
Step 3/5 : MAINTAINER James Alexander Clark <james.clark@ligo.org>
 ---> Using cache
 ---> 944ff891de2f
Step 4/5 : RUN mkdir -p /cvmfs /hdfs /hadoop /etc/condor
 ---> Using cache
 ---> 28ca2acdc65c
Step 5/5 : ENTRYPOINT ["/bin/bash"]
 ---> Using cache
 ---> 00198765765d
Successfully built 00198765765d
Successfully tagged containers.ligo.org/james-clark/osg-demos/lalsuite-v6.53:latest
➜  lalapps_frview git:(master) ✗ docker push containers.ligo.org/james-clark/osg-demos/lalsuite-v6.53:latest 
The push refers to repository [containers.ligo.org/james-clark/osg-demos/lalsuite-v6.53]
6c23c25f0d7b: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
042ec620bdd5: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
b832d3a5d424: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
b6ea637f4d28: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
5ca4bc2f28f4: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
b3bedac7e6a8: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
93c7909efead: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
f4dbacc20442: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
154d871cccd2: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
cc5852a2fbb9: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
c581f4ede92d: Mounted from james-clark/osg-demos/lalsuite/lalsuite-v6.53 
latest: digest: sha256:139312cbe69ba637e523bf2e568935f109f5481ac757c1df8f97216a99257990 size: 2629
```
